import Scrollbar from 'smooth-scrollbar';
import OverscrollPlugin from 'smooth-scrollbar/plugins/overscroll';

// include JSON data

var data = (function () {
	var json = null;
	$.ajax({
			'async': false,
			'global': false,
			'url': './js/data.json',
			'dataType': "json",
			'success': function (data) {
					json = data;
			}
	});
	return json;
})();

// Overscroll plugin for scrollbar

Scrollbar.use(OverscrollPlugin);

$(function() {
  // Scrollbar init and setting

	const scrollSelector = document.querySelector('#main-scrollbar');
	const scrollOptions = {
		damping: "0.075",
		thumbMinSize: 10,
		overscrollEffect: true
	};

	const scrollbar = Scrollbar.init(scrollSelector, scrollOptions);

	scrollbar.updatePluginOptions('overscroll', {effect: 'glow'});

	// Anchor smooth scroll

	const anchorClickLink = $('a.anchor-link');
	const nav = $('nav.desktop');

	anchorClickLink.click(function (event) {
		const fullNameLink = event.currentTarget.attributes.href.value;
		const shortNameLink = fullNameLink.slice(1, fullNameLink.lenght);
		const targetElement = $('a[name="' + shortNameLink + '"]');
		const targetOffsetTop = targetElement["0"].offsetTop - nav.outerHeight();
		const anchorSpeed = 1500;

		scrollbar.scrollTo(0, targetOffsetTop, anchorSpeed);
	});

	// Nav functions

	const hamburger = $('.hamburger');
	const page = $('#main-scrollbar');
	const bgDark = $('.bg-dark');
	const navItem = $('nav ul a');

	hamburger.click(function() {
		if (nav.hasClass('open')) {
			hamburger.removeClass('open');
			nav.removeClass('open');
			page.removeClass('open-nav');
			bgDark.removeClass('open-nav');
		} else {
			hamburger.addClass('open');
			nav.addClass('open');
			page.addClass('open-nav');
			bgDark.addClass('open-nav');
		}
	});

	navItem.click(function() {
		if (nav.hasClass('open')) {
			hamburger.removeClass('open');
			nav.removeClass('open');
			page.removeClass('open-nav');
			bgDark.removeClass('open-nav');
		}
	});

	function showMobileNav() {
		if (window.innerWidth > 1200) {
			nav.removeClass('mobile');
		} else {
			nav.addClass('mobile');
		}
	};

	showMobileNav();

	$(window).resize(function() {
		showMobileNav();
	});

	// Modal feedback

	const modal = $('#modal');
	const modalBtnClose = $('#modal i.close');
	const modalBtnOpen = $('.btn.modal');

	modalBtnOpen.click(function() {
		modal.addClass('open');
	});

	modalBtnClose.click(function() {
		modal.removeClass('open');
	});

	// Header info block

	const info = $('#header .info');
	const infoBtn = $('#header .hide-info-header');

	infoBtn.click(function() {
		info.toggleClass('hide');

		if (window.innerWidth < 768) {
			nav.toggleClass('white');
		}
	});

	// Listener scroll

	if (scrollbar.scrollTop > 100) {
		nav.addClass('scroll');
	} else {
		nav.removeClass('scroll');
	}

	scrollbar.addListener(function (status) {
		if (scrollbar.scrollTop > 100) {
			nav.addClass('scroll');
		} else {
			nav.removeClass('scroll');
		}
	});

	// Sending form

		// Programm

		const formProgramm = $('#form-full-programm');
		const btnProgramm = $('#form-full-programm .submit');

		btnProgramm.click(function (e) {
			formProgramm.submit();
		});

		// Feedback

		const formFeedback = $('#feedback-form');
		const btnFeedback = $('#feedback-form .submit');

		btnFeedback.click(function (e) {
			formFeedback.submit();
		});

		// Modal feedback

		const formModal = $('#modal-form');
		const btmModal = $('#modal-form .submit');

		btmModal.click(function (e) {
			formModal.submit();
		});

	// Program list more btn

	const listBtn = $('section.program-list .more');
	const listProgram = $('section.program-list article');

	listBtn.click(function(e) {
		const info = $(`.text.${e.target.parentElement.classList[1]} .info`);

		if (info.hasClass('open')) {
			info.removeClass('open');
			e.target.innerText = "Детальніше";
		} else {
			info.addClass('open');
			e.target.innerText = "Закрити";
		}
	});

	listProgram.hover(
		function(e) {
			$(`.text.${e.currentTarget.classList[0]} .info`).addClass('open');
			e.currentTarget.children[0].lastElementChild.innerText = "Закрити";
		},
		function(e) {
			$(`.text.${e.currentTarget.classList[0]} .info`).removeClass('open');
			e.currentTarget.children[0].lastElementChild.innerText = "Детальніше";
		}
	);

	// Advantages

	const advantagesBtn = $('#advantages .detail');

	advantagesBtn.click(function(e) {
		const parentClass = e.target.parentElement.classList[1];
		const paragraph = $(`.item.${parentClass} p.info`);

		if (paragraph.hasClass('open')) {
			paragraph.removeClass('open');
			e.target.innerText = "Детальніше";
		} else {
			paragraph.addClass('open');
			e.target.innerText = "Закрити";
		}
	});

	// Video Gallery

	$('#video .slider .items').lightGallery({
		loadYoutubeThumbnail: true,
		youtubeThumbSize: 'default',
		loadVimeoThumbnail: true,
		vimeoThumbSize: 'thumbnail_medium',
	});

	$('#video .slider .items').slick({
		infinite: true,
		arrows: true,
		prevArrow: $('#video .controls .prev'),
		nextArrow: $('#video .controls .next'),
		draggable: false,
		speed: 300,
		slidesToShow: 3,
		slidesToScroll: 1,
		responsive: [
			{
				breakpoint: 1200,
				settings: {
					slidesToShow: 2,
				}
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 1,
				}
			}
		]
	});

	$('#team .slider .items').slick({
		infinite: false,
		arrows: true,
		prevArrow: $('#team .controls .prev'),
		nextArrow: $('#team .controls .next'),
		draggable: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1,
		mobileFirst: true,
		responsive: [
			{
				breakpoint: 1200,
				settings: "unslick"
			},
			{
				breakpoint: 767,
				settings: {
					slidesToShow: 2
				}
			}
		]
	});

	$('#date .slider').slick({
		infinite: false,
		arrows: true,
		prevArrow: $('#date .controls .prev'),
		nextArrow: $('#date .controls .next'),
		draggable: false,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1
	});

	// Schedule init slider

	$('#schedule .time .container').slick({
		infinite: true,
		arrows: true,
		prevArrow: $('#schedule .controls .prev'),
		nextArrow: $('#schedule .controls .next'),
		draggable: true,
		speed: 300,
		slidesToShow: 1,
		slidesToScroll: 1
	});

	// Preloader

	const preloader = $('.preloader');
	preloader.addClass('hide');

	// Video BG

	const videoBG = $('#video-bg');

	videoBG[0].play();
});